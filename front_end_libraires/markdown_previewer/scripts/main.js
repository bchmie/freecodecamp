'use strict';

const DEFAULT_TEXT = `
# This is an interactive markdown editor
## Here you can type whatever you want
[Here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#links) you can find out how to use markdown\n
Backticks are used for \`a line of code\`\n
Also a code block can be done using markdown:
\`\`\`python
For nice_feture in markdown:
    let_the_user_know(feature)
\`\`\`

Main markdown advantages:
  * Easy
  * Quick
  * Lightweigh

> Blockquotes are very handy in email to emulate reply text

You can even put here an image: ![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

**This** is very nice, right?
`;

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { input: DEFAULT_TEXT};
        this.updateInput = this.updateInput.bind(this);
        this.updatePreview = this.updatePreview.bind(this);
    }

    updateInput() {
        this.setState({ input: event.target.value });
    }

    updatePreview() {
        return {__html: marked(this.state.input)};
    }

    render() {
        return (
            <div id="container">
                <textarea id="editor" class="display" defaultValue={ this.state.input } onChange={this.updateInput} />
                <div id="preview" class="display" dangerouslySetInnerHTML={ this.updatePreview() } />
            </div>
        );
    }
}

let domContainer = document.querySelector('#container-external');
ReactDOM.render(<App />, domContainer);