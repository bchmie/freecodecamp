'use strict';
const bank1 = [
    {
        name: 'Heater1',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3',
        key: 'Q',
        keycode: 81,
    },
    {
        name: 'Heater2',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3',
        key: 'W',
        keycode: 87,
    },
    {
        name: 'Heater3',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3',
        key: 'E',
        keycode: 69,
    },
    {
        name: 'Heater4-1',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3',
        key: 'A',
        keycode: 65,
    },
    {
        name: 'Heater6',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3',
        key: 'S',
        keycode: 83,
    },
    {
        name: 'OpenHH',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3',
        key: 'D',
        keycode: 68,
    },
    {
        name: 'KicknHat',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3',
        key: 'Z',
        keycode: 90,
    },
    {
        name: 'Kick',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3',
        key: 'X',
        keycode: 88,
    },
    {
        name: 'ClosedHH',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3',
        key: 'C',
        keycode: 67,
    },
    
]

const bank2 = [
    {
        name: 'Chord-1',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_1.mp3',
        key: 'Q',
        keycode: 81,
    },
    {
        name: 'Chord-2',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_2.mp3',
        key: 'W',
        keycode: 87,
    },
    {
        name: 'Chord-3',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_3.mp3',
        key: 'E',
        keycode: 69,
    },
    {
        name: 'Shaker',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Give_us_a_light.mp3',
        key: 'A',
        keycode: 65,
    },
    {
        name: 'Open-HH',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Dry_Ohh.mp3',
        key: 'S',
        keycode: 83,
    },
    {
        name: 'Closed-HH',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Bld_H1.mp3',
        key: 'D',
        keycode: 68,
    },
    {
        name: 'Punchy-Kick',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/punchy_kick_1.mp3',
        key: 'Z',
        keycode: 90,
    },
    {
        name: 'Side-Stick',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/side_stick_1.mp3',
        key: 'X',
        keycode: 88,
    },
    {
        name: 'Snare',
        url: 'https://s3.amazonaws.com/freecodecamp/drums/Brk_Snr.mp3',
        key: 'C',
        keycode: 67,
    },
  
]


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display: 'START',
            powered: true,
            bank: bank1,
            volume: 0.1,
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleOnOffButton = this.handleOnOffButton.bind(this);
        this.handleBankButton = this.handleBankButton.bind(this);
        this.handleVolumeChange = this.handleVolumeChange.bind(this);
    }

    handleOnOffButton() {
        this.setState({
            powered: this.state.powered ? false : true,
            display: this.state.powered ? 'Power OFF' : 'Power ON',
        });
    }

    handleBankButton() {
        this.setState({
            bank: this.state.bank === bank1 ? bank2 : bank1,
            display: this.state.bank === bank1 ? 'Bank 2' : 'Bank 1',
        })
    }

    handleVolumeChange() {
        this.setState({
            display: 'Volume: ' + event.target.value * 100,
            volume: event.target.value,
        })
    }

    handleClick(i) {
        if (!this.state.powered) {
            return;
        }
        let audio = document.getElementById(bank1[i -1]['key']);
        audio.play();
        this.setState({
            display: this.state.bank[i - 1]['name'],
        });
    }

    handleKeyPress(event) {
        for (let j = 0; j < this.state.bank.length; j++) {
            if (event.keyCode === this.state.bank[j]['keycode']) {
                this.handleClick(j + 1);
            }
        }
    }

    componentWillMount() {
        document.addEventListener('keydown', this.handleKeyPress);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleKeyPress)
    }

    render() {
        return (
            <div id="drums-external">
                    <Drums onClick={this.handleClick} />
                    <ControlPanel
                        display={this.state.display}
                        handleOnOffButton={this.handleOnOffButton}
                        handleBankButton={this.handleBankButton}
                        handleVolumeChange={this.handleVolumeChange}
                        volume={this.state.volume}
                    />
            </div>
        )
    }
}


class Drums extends React.Component {
    renderDrum(i) {
        return(
            <Drum 
                className="drum-pad"
                i={i}
                onClick={this.props.onClick}
            />
        )
    }

    render() {
        return(
            <div id="drum-machine">
                {this.renderDrum(1)}
                {this.renderDrum(2)}
                {this.renderDrum(3)}
                {this.renderDrum(4)}
                {this.renderDrum(5)}
                {this.renderDrum(6)}
                {this.renderDrum(7)}
                {this.renderDrum(8)}
                {this.renderDrum(9)}
            </div>
        )
    }
}


class Drum extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <button className="drum-pad" id={bank1[this.props.i -1]['name']} onClick={() => this.props.onClick(this.props.i)}>
                {bank1[this.props.i - 1]['key']}
                <audio className="clip" id={bank1[this.props.i -1]['key']} preload="auto" src={bank1[this.props.i -1]['url']} />
            </button>
        )
    }
}


class ControlPanel extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <div className="control-panel">
                    <div id="display">{this.props.display}</div>
                    <div className="buttons">
                        <button onClick={() => this.props.handleOnOffButton()}>Power</button>
                        <button onClick={() => this.props.handleBankButton()}>BANK</button>
                    </div>
                    <input className="volume-slider" type="range" min="0" max="1" step="0.01" value={this.props.volume} onChange={() => this.props.handleVolumeChange()}/>
            </div>
        )
    }
}


let domContainer = document.querySelector('.external-container');
ReactDOM.render(<App />, domContainer);