$(document).ready(function() {
    getRandomQuote();
    let color = getRandomColor();
    changeMainColor(color);
})

function getRandomQuote() {
    $.getJSON('https://api.forismatic.com/api/1.0/?method=getQuote&lang=en&format=jsonp&jsonp=?', function(json) {
        $('#quote-text').text(json.quoteText);
        $('#quote-author').text('- ' + json.quoteAuthor);
    })
}

function getRandomColor() {
    let rgb_str = 'rgb(' + Math.round((Math.random() * 255)).toString() +
    ', ' + Math.round((Math.random() * 255)).toString() +
    ', ' + Math.round((Math.random() * 255)).toString() + ')';
    return rgb_str;
}

function changeMainColor(color) {
    $('.colorized').css('color', color);
    $('.background-colorized').css('background-color', color);
}

function getNewQuote() {
    getRandomQuote();
    let color = getRandomColor();
    changeMainColor(color);
}
