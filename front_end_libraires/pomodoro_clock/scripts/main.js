class App extends React.Component {
    constructor(props) {
        super(props);
        this.alarmBeep = new Audio('https://goo.gl/65cBl1');
        this.state = {
            isRunning: false,
            isPaused: undefined,
            isSessionNow: true,
            session_time: 1500,
            break_time: 300,
            time: undefined,
        }
        this.tick = this.tick.bind(this);
        this.startStop = this.startStop.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.changeBreakTimer = this.changeBreakTimer.bind(this);
        this.changeSessionTimer = this.changeSessionTimer.bind(this);
    }

    tick() {
        if(this.state.time === 0) {
            this.setState({
                isSessionNow: this.state.isSessionNow ? false : true,
                time: this.state.isSessionNow ? this.state.break_time : this.state.session_time,
            });
            this.alarmBeep.play();
            return;
        }
        this.setState({
            time: this.state.time - 1,
        });
    }

    startStop() {
        if (!this.state.isRunning) {
            this.setState({
                isRunning: true,
                isPaused: false,
                time: this.state.isSessionNow ? this.state.session_time : this.state.break_time - 1,
            });
            this.timer = setInterval(() => this.tick(), 1000);
        } else if (this.state.isPaused) {
            this.setState({
                isRunning: true,
                isPaused: false,
            });
            this.timer = setInterval(() => this.tick(), 1000);
        } else {
            clearInterval(this.timer);
            this.setState({
                isPaused: true,
            })
        }
    }

    resetTimer() {
        this.alarmBeep.pause()
        this.alarmBeep.currentTime = 0;
        clearInterval(this.timer);
        this.setState({
            session_time: 1500,
            break_time: 300,
            isSessionNow: true,
            time: this.state.session_time,
            isRunning: false,
            isPaused: false,
        })
    }

    changeBreakTimer(delta) {
        if(this.state.break_time + delta <= 0 || this.state.break_time + delta > 3600) {
            return;
        }
        this.setState({
            break_time: this.state.break_time + delta,
        });
    }

    changeSessionTimer(delta) {
        if(this.state.session_time + delta <= 0 || this.state.session_time + delta > 3600) {
            return;
        }
        this.setState({
            session_time: this.state.session_time + delta,
        });
    }

    displayTime(seconds) {
        let time = new Date(null);
        time.setSeconds(seconds);
        return time.toISOString().substr(14, 5)
    }

    render() {
        return(
            <div id="app">
                <h1>Pomodoro clock</h1>
                <div id="timer">
                    <h2 id="timer-label">{this.state.isSessionNow ? 'Session:' : 'Break:'}</h2>
                    <h2 id="time-left">{this.state.isRunning ? this.displayTime(this.state.time) : this.state.isSessionNow ? this.displayTime(this.state.session_time) : this.displayTime(this.state.break_time) }</h2>
                </div>
                <div>
                    <button id="start_stop" className="timer-button" onClick={this.startStop}><i className={(!this.state.isRunning || this.state.isPaused) ? "fas fa-play fa-2x" : "fas fa-pause fa-2x"}></i></button>
                    <button id="reset" className="timer-button" onClick={this.resetTimer}><i className="fas fa-redo-alt fa-2x"></i></button>
                </div>
                <br /><br />
                <div id="session-label">
                    <p>Session length: </p>
                    <button id="session-increment" onClick={() => this.changeSessionTimer(60)}><i className="far fa-plus-square fa-2x"></i></button>
                    <h3 id="session-length">{Math.floor(this.state.session_time/60)}</h3>
                    <button id="session-decrement" onClick={() => this.changeSessionTimer(-60)}><i className="far fa-minus-square fa-2x"></i></button>
                </div>
                <div id="break-label">
                    <p>Break length: </p>
                    <button id="break-increment" onClick={() => this.changeBreakTimer(60)}><i className="far fa-plus-square fa-2x"></i></button>
                    <h3 id="break-length">{Math.floor(this.state.break_time/60)}</h3>
                    <button id="break-decrement" onClick={() => this.changeBreakTimer(-60)}><i className="far fa-minus-square fa-2x"></i></button>
                </div>
                <audio id="beep" preload="auto" src="https://goo.gl/65cBl1" ref={(audio) => {this.alarmBeep = audio; }} />
            </div>
        );
    };
}


let domContainer = document.querySelector('.external-container');
ReactDOM.render(<App />, domContainer);
