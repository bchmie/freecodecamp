class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentNum: [],
            operations: [],
        };
        this.handleNum = this.handleNum.bind(this);
        this.handleOperation = this.handleOperation.bind(this);
        this.handleEvaluate = this.handleEvaluate.bind(this);
    }

    displayOperations() {
        if (this.state.currentNum.length === 0 && this.state.operations.length === 0) {
            return 0 // if there is nothing to display, display 0
        }
        return(this.state.operations.join(' ') + this.state.currentNum.join(''))
    }

    handleClear() {
        this.setState({
            currentNum: [],
            operations: [],
        });
    }

    handleNum(i) {
        if (i === 0 && this.state.currentNum[0] === 0) {
            return;  // do not let multiple leading zeros
        }
        this.setState({
            currentNum: [...this.state.currentNum, i],
        });
    }

    handleDecimal() {
        if (this.state.currentNum.indexOf('.') !== -1) {
            return; // do not let multiple leading decimals
        } else {
            this.setState({
                currentNum: [...this.state.currentNum, '.'],
            });
        }

    }

    handleOperation(operand) {
        let operations = this.state.operations;
        let curNum = this.state.currentNum;

        // SPECIAL CASE #1
        // if operand is a first operation, only allow for - as a curNum
        if(operations.length === 0 && curNum.length === 0) {
            console.log('sc1');
            if(operand === '-') {
                this.setState({
                    currentNum: ['-'],
                });
                return;
            } else {
                return;
            };
        // SPECIAL CASE #2
        //if last operand is * or /, allow to begin with a negative number
        } else if('*/'.indexOf(operations[operations.length -1]) !== -1 && operand === '-' && curNum.length === 0) {
            console.log('sc2');
            this.setState({
                currentNum: ['-'],
            });
            return;
        // SPECIAL CASE #3
        // if curnum is only '-' and follows / or *, setting another operand changes both - and * /
        } else if ('*/'.indexOf(operations[operations.length -1]) !== -1 && curNum[0] === '-' && curNum.length === 1) {
            console.log('sc3');
            operations.splice(-1);
            this.setState({
                operations: [...operations, operand],
                currentNum: [],
            });
            return;
        }

        // if currNum, add current number and operand
        if(curNum.length > 0) {
            curNum = parseFloat(curNum.join(''));
            this.setState({
                operations: [...operations, curNum, operand],
                currentNum: []
            });
            return;
        // if currNum is empty, replace last operand
        } else if (curNum.length === 0 && '+-*/'.indexOf(operations[operations.length -1]) !== -1) {
            operations.splice(-1);
            this.setState({
                operations: [...operations, operand],
            });
        } else {
            this.setState({
                operations: [...operations, operand],
            });
        }
        return;
    }



    handleEvaluate() {
        let operations = this.state.operations;
        let curNum = this.state.currentNum;
        
        // if curNum is a number, add it as a last element in operations
        if ((curNum[0] === '-' && curNum.length > 1) || (curNum[0] !== '-' && curNum.length > 0)) {
            let num = parseFloat(curNum.join(''));
            operations.push(num);
        };

        // remove any leading operations
        if('*/+-'.indexOf(operations[operations.length - 1]) !== -1) {
            operations.splice(operations.length -1, 1);
        };

        // multiplication and division first
        for (let i = 0; i < operations.length; i++) {
            if (operations[i] === '*' || operations[i] === '/') {
                let a = operations[i - 1];
                let b = operations[i + 1];
                
                let product = operations[i] === '*' ? a * b : a / b;
                operations.splice(i - 1, 3, product);
                i = i - 1;
            }
        }

        // then add and subtract
        for (let i = 0; i < operations.length; i++) {
            if (operations[i] === '+' || operations[i] === '-') {
                let a = operations[i - 1];
                let b = operations[i + 1];
                let product = operations[i] === '+' ? a + b : a - b;
                operations.splice(i - 1, 3, product);
                i = i - 1;
            }
        };

        this.setState({
            operations: operations,
            currentNum: [],
        });
    }

    render() {
        return(
            <div id="calculator">
                <div id="display">{this.displayOperations()}</div>
                <button id="clear" onClick={() => this.handleClear()}>AC</button>
                <button id="divide" onClick={() => this.handleOperation('/')}>/</button>
                <button id="multiply" onClick={() => this.handleOperation('*')}>x</button>
                <button id="subtract" onClick={() => this.handleOperation('-')}>-</button>
                <button id="add" onClick={() => this.handleOperation('+')}>+</button>
                <button id="equals" onClick={() => this.handleEvaluate()}>=</button>
                <button id="one" onClick={() => this.handleNum(1)}>1</button>
                <button id="two" onClick={() => this.handleNum(2)}>2</button>
                <button id="three" onClick={() => this.handleNum(3)}>3</button>
                <button id="four" onClick={() => this.handleNum(4)}>4</button>
                <button id="five" onClick={() => this.handleNum(5)}>5</button>
                <button id="six" onClick={() => this.handleNum(6)}>6</button>
                <button id="seven" onClick={() => this.handleNum(7)}>7</button>
                <button id="eight" onClick={() => this.handleNum(8)}>8</button>
                <button id="nine" onClick={() => this.handleNum(9)}>9</button>
                <button id="zero" onClick={() => this.handleNum(0)}>0</button>
                <button id="decimal" onClick={() => this.handleDecimal()}>.</button>
            </div>
        )
    }
}


let domContainer = document.querySelector('.external-container');
ReactDOM.render(<App />, domContainer);